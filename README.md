## *Šifrování AES*

### Implementační platforma

[Přípravek Digilent Basys 3](basys3.adoc) 

### Popis chování

V klidovém stavu obvod čeká na zadání 128 bitového plaintextu. Zadávání se provádí po bytech pomocí přepínačů SWITCH(7:0, W13-V17). Byte je načten při stisku tlačítka BTN0(W19-BTNL). Další byte je načten při stisku tlačítka BTN1(T17-BTNR). Další byte je načten opět při stisku tlačítka BTN0, atd. (tlačítka BTN0 a BTN1 se střídají). Byty jsou zadávány od nejvyššího k nejnižšímu (zleva doprava).

Po zadání posledního (šestnáctého) bytu se provede šifrování a výsledný ciphertext je zobrazen na sedmi segmentovém displeji. Vzhledem k tomu, že displej je schopen zobrazit pouze 4 šestnáctkové číslice (tj. 16 bitů), výsledek rozdělen do osmi skupin po 16 bitech a pomocí přepínačů SWITCH(15:13, R2-U1) se vybírá příslušná skupina.

Klíč pro šifrování je konstantní x"00112233445566778899AABBCCDDEEFF" a je uložen v package KEY.vhd, kde se dá změnit.

AD: Při zadávání plaintextu se na LED diodách LED(15:0, L1-U16) zobrazuje posledních 16 bitů zadávaného plaintextu pro kontrolu.

### Schémata

<table>
  <tr>
    <td>Blokové schéma datové cesty  </td>
    <td>Graf přechodů a výstupů řadiče</td>
  </tr>
  <tr>
    <td><img src="datapath.jpg" width=500 align="centre" ></td>
    <td><img src="controller.jpg" width=500 align="centre"></td>
  </tr>
 </table>

### Popis souborů

| Soubor |  Popis |
| ---------- | -------- |
| [***daneklu2_AES.xpr.zip***](daneklu2_AES.xpr.zip) | ***Archiv s Vivado Projektem*** |
| [AES_WHOLE.vhd](AES/AES_WHOLE.vhd) | top-level entita + displej (pro validaci - nahrání do přípravku) |
| [AES.vhd](AES/AES.vhd) | top-level entita (pro verifikaci) |
| [DATAPATH.vhd](AES/DATAPATH.vhd)  | datová jednotka |
| [CONTROLLER.vhd](AES/CONTROLLER.vhd)  | řídící jednotka |
| [8SHIFT_REG.vhd](AES/8SHIFT_REG.vhd)  | posuvný registr o osm bitů s resetem |
| [REG.vhd](AES/REG.vhd)   | registr s resetem |
| [FLIP_FLOP.vhd](AES/FLIP_FLOP.vhd)    | D pro správné snímání BTN0 a BTN1 |
| [MUX.vhd](AES/MUX.vhd)    | multiplexor se 2 vstupy |
| [SBOX.vhd](SBOX.vhd) | substituce pro jeden byte |
|[GF_MULT.vhd](AES/GF_MULT.vhd) | násobení dvěma v GF |
|[MAKE_T.vhd](AES/MAKE_T.vhd) | vytvoření základního 32-bit slova pro vytvoření dalšího klíče |
|[MIX_COL.vhd](AES/MIX_COL.vhd) | prohození bytů ve sloupci |
|[MIX_COLS.vhd](AES/MIX_COLS.vhd) | prohození všech sloupců |
|[SHIFT_RWS.vhd](AES/SHIFT_RWS.vhd) | shift řádků |
|[SUB.vhd](AES/SUB.vhd) | substituce celého 128-bit vstupu |
|[hex2seg.vhd](AES/hex2seg.vhd) | převodník na sedmi segmentový displej |
|[KEY_EXPAND.vhd](AES/KEY_EXPAND.vhd) | rozšíření klíče |
|[KEY.vhd](AES/KEY.vhd) | package s konstantou klíče |
|[TB_AES_BLOCK.vhd](AES/TB_AES_BLOCK.vhd) | testbench pro AES |
|[AES_WHOLE.xdc](AES/AES_WHOLE.xdc) | přiřazení pinů |
|[plaintext.vec](plaintext.vec) | vstupy pro testbench |
|[cifertext.vec](cifertext.vec) | správné výstupy pro kontrolu v testbench |