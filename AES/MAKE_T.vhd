library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity MAKE_T is
port (
    INPUT  : in  STD_LOGIC_VECTOR (31 downto 0);
    ROUND  : in  STD_LOGIC_VECTOR ( 3 downto 0);
    OUTPUT : out STD_LOGIC_VECTOR (31 downto 0)
);
end MAKE_T;

architecture MAKE_T_BODY of MAKE_T is
    signal SHIFTED, SUB_SHIFTED, SUB_SHIFTED_T : STD_LOGIC_VECTOR (31 downto 0);
    
    component SBOX is
    port (
        INPUT  : in  STD_LOGIC_VECTOR (7 downto 0);
        OUTPUT : out STD_LOGIC_VECTOR (7 downto 0)
    );
    end component SBOX;

begin
    SHIFT : process (INPUT)
    begin
        SHIFTED (31 downto 8) <= INPUT (23 downto  0);
        SHIFTED ( 7 downto 0) <= INPUT (31 downto 24);
    end process SHIFT;
    -- prirazeni jak v SUB tak v RC_XOR do SUB_SHIFTED
    SUB : for I in 1 to 4 generate
        SUB_BYTE: SBOX
            port map (
                INPUT  => SHIFTED    (I*8 - 1 downto (I - 1)*8),
                OUTPUT => SUB_SHIFTED(I*8 - 1 downto (I - 1)*8)
            );
    end generate SUB;
    
   
   RC_XOR : process (ROUND, SUB_SHIFTED)--jen round ??
   begin
       SUB_SHIFTED_T <= SUB_SHIFTED;
       case ROUND is
           when "0001" => SUB_SHIFTED_T(31 downto 24) <= SUB_SHIFTED(31 downto 24) xor "00000001";
           when "0010" => SUB_SHIFTED_T(31 downto 24) <= SUB_SHIFTED(31 downto 24) xor "00000010";
           when "0011" => SUB_SHIFTED_T(31 downto 24) <= SUB_SHIFTED(31 downto 24) xor "00000100";
           when "0100" => SUB_SHIFTED_T(31 downto 24) <= SUB_SHIFTED(31 downto 24) xor "00001000";
           when "0101" => SUB_SHIFTED_T(31 downto 24) <= SUB_SHIFTED(31 downto 24) xor "00010000";
           when "0110" => SUB_SHIFTED_T(31 downto 24) <= SUB_SHIFTED(31 downto 24) xor "00100000";
           when "0111" => SUB_SHIFTED_T(31 downto 24) <= SUB_SHIFTED(31 downto 24) xor "01000000";
           when "1000" => SUB_SHIFTED_T(31 downto 24) <= SUB_SHIFTED(31 downto 24) xor "10000000";
           when "1001" => SUB_SHIFTED_T(31 downto 24) <= SUB_SHIFTED(31 downto 24) xor "00011011";
           when "1010" => SUB_SHIFTED_T(31 downto 24) <= SUB_SHIFTED(31 downto 24) xor "00110110";
           when others => null;
       end case;
    end process RC_XOR;
    
    FINAL_T : OUTPUT <= SUB_SHIFTED_T;
    
    
end MAKE_T_BODY;
