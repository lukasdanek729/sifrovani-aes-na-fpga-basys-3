library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity SHIFT_8_REG is
port (
    INPUT        : in  STD_LOGIC_VECTOR (7 downto 0);
    LOAD         : in  STD_LOGIC;
    RESET        : in  STD_LOGIC;
    CLK          : in  STD_LOGIC;
    INPUT_LAST16 : out STD_LOGIC_VECTOR ( 15 downto 0);
    OUTPUT       : out STD_LOGIC_VECTOR (127 downto 0)
);
end SHIFT_8_REG;

architecture SHIFT_8_REG_BODY of SHIFT_8_REG is
    signal CURRENT_STATE : STD_LOGIC_VECTOR(127 downto 0);
begin
    -- not sure    
    SHIFT : process (CLK)
    begin
        if CLK = '1' and CLK'event then
            if RESET = '1' then
                CURRENT_STATE <= (others => '0');
            elsif LOAD = '1' then
                CURRENT_STATE (127 downto 8) <= CURRENT_STATE (119 downto 0);
                CURRENT_STATE (  7 downto 0) <= INPUT      (  7 downto 0);
            end if;
        end if;
    end process SHIFT;
    INPUT_LAST16 <= CURRENT_STATE(15 downto 0);
    OUTPUT       <= CURRENT_STATE;
end SHIFT_8_REG_BODY;
