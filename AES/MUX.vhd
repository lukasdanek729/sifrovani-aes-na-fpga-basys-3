library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity MUX is
port (
    INPUT_0 : in  STD_LOGIC_VECTOR(127 downto 0);
    INPUT_1 : in  STD_LOGIC_VECTOR(127 downto 0);
    SEL     : in  STD_LOGIC;
    OUTPUT  : out STD_LOGIC_VECTOR(127 downto 0)
);
end MUX;

architecture MUX_BODY of MUX is
begin

    MUX : process (SEL, INPUT_0, INPUT_1)
    begin
        if SEL = '0' then
            OUTPUT <= INPUT_0;
        else
            OUTPUT <= INPUT_1;
        end if;
    end process MUX;
end MUX_BODY;
