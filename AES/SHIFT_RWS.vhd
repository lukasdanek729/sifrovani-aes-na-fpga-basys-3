library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity SHIFT_RWS is
port (
    INPUT  : in  STD_LOGIC_VECTOR (127 downto 0);
    OUTPUT : out STD_LOGIC_VECTOR (127 downto 0)
);
end SHIFT_RWS;

architecture SHIFT_RWS_BODY of SHIFT_RWS is
begin
    SHIFT : process (INPUT)
    begin
        OUTPUT (127 downto 120) <= INPUT (127 downto 120); -- 0 <- 0
        OUTPUT (119 downto 112) <= INPUT ( 87 downto  80); -- 1 <- 5
        OUTPUT (111 downto 104) <= INPUT ( 47 downto  40); -- 2 <-10
        OUTPUT (103 downto  96) <= INPUT (  7 downto   0); -- 3 <-15
        OUTPUT ( 95 downto  88) <= INPUT ( 95 downto  88); -- 4 <- 4
        OUTPUT ( 87 downto  80) <= INPUT ( 55 downto  48); -- 5 <- 9
        OUTPUT ( 79 downto  72) <= INPUT ( 15 downto   8); -- 6 <-14
        OUTPUT ( 71 downto  64) <= INPUT (103 downto  96); -- 7 <- 3
        OUTPUT ( 63 downto  56) <= INPUT ( 63 downto  56); -- 8 <- 8
        OUTPUT ( 55 downto  48) <= INPUT ( 23 downto  16); -- 9 <-13
        OUTPUT ( 47 downto  40) <= INPUT (111 downto 104); --10 <- 2
        OUTPUT ( 39 downto  32) <= INPUT ( 71 downto  64); --11 <- 7
        OUTPUT ( 31 downto  24) <= INPUT ( 31 downto  24); --12 <-12
        OUTPUT ( 23 downto  16) <= INPUT (119 downto 112); --13 <- 1
        OUTPUT ( 15 downto   8) <= INPUT ( 79 downto  72); --14 <- 6
        OUTPUT (  7 downto   0) <= INPUT ( 39 downto  32); --15 <-11
    end process SHIFT;
end SHIFT_RWS_BODY;
