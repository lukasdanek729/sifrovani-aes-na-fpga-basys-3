library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use STD.TEXTIO.all;    

entity TB_AES_BLOCK is
end TB_AES_BLOCK;

architecture TB_AES_BLOCK_BODY of TB_AES_BLOCK is
    component AES is
    port (
        CLK          : in  STD_LOGIC; 
        RESET        : in  STD_LOGIC;
        BTN0         : in  STD_LOGIC;
        BTN1         : in  STD_LOGIC;
        INPUT        : in  STD_LOGIC_VECTOR (  7 downto 0);
        INPUT_LAST16 : out STD_LOGIC_VECTOR ( 15 downto 0);
        OUTPUT       : out STD_LOGIC_VECTOR (127 downto 0)
    );
    end component AES;
    
    signal  TB_CLK             : STD_LOGIC;
    signal  TB_RESET           : STD_LOGIC;
    signal  TB_BTN0            : STD_LOGIC;
    signal  TB_BTN1            : STD_LOGIC;
    signal  TB_INPUT           : STD_LOGIC_VECTOR (  7 downto 0);
    signal  TB_INPUT_LAST16    : STD_LOGIC_VECTOR ( 15 downto 0);
    signal  TB_OUTPUT          : STD_LOGIC_VECTOR (127 downto 0);
    constant CLK_PERIOD : TIME              := 10 ns;
begin

    DUT : AES 
        port map (
            CLK           => TB_CLK    ,
            RESET         => TB_RESET  ,
            BTN0          => TB_BTN0   ,
            BTN1          => TB_BTN1   ,
            INPUT         => TB_INPUT  ,
            INPUT_LAST16  => TB_INPUT_LAST16  ,
            OUTPUT        => TB_OUTPUT
        );

     CLK_GEN : process
     begin
        TB_CLK <= '0';
        wait for CLK_PERIOD/2;
        TB_CLK <= '1';
        wait for CLK_PERIOD/2;
     end process CLK_GEN;

    INPUT_GEN : process
        file     PL_TXT           : TEXT is in "C:\Users\ludan\PNO\AES_Encryption\plaintext.vec";
        file     CF_TXT           : TEXT is in "C:\Users\ludan\PNO\AES_Encryption\cifertext.vec";
        variable LINE_PL, LINE_CF : LINE;
        variable PL_BV, CF_BV     : BIT_VECTOR       (127 downto 0);
        variable PL, CF           : STD_LOGIC_VECTOR (127 downto 0);
    begin
        -- testujeme vsechny vstupni hodnoty ze souboru
        while not ENDFILE(PL_TXT) loop
            readline(PL_TXT, LINE_PL);            
            readline(CF_TXT, LINE_CF);
            read(LINE_PL, PL_BV);
            read(LINE_CF, CF_BV);
            PL := To_StdLogicVector(PL_BV);
            CF := To_StdLogicVector(CF_BV);    
            --zresetujeme registry    
            TB_RESET <= '1';   
            wait until TB_CLK = '1';
            wait for CLK_PERIOD*2;
            TB_RESET <= '0';
            --nacitani 16 bajtu   
            for I in 0 to 15 loop
                TB_INPUT <= PL(128 - I*8 -1 downto 128 - (I+1)*8);
                wait until TB_CLK = '0';
                if I/2 = (I + 1)/2 then
                    TB_BTN0 <= '1';
                else
                    TB_BTN1 <= '1';
                end if;
                wait until TB_CLK = '1';
                wait for CLK_PERIOD/4;
                if I/2 = (I + 1)/2 then
                    TB_BTN0 <= '0';
                else
                    TB_BTN1 <= '0';
                end if;
            end loop;  
            --cekame nez se projede 10 rund
            wait for 11*CLK_PERIOD;                          -- pockame na vysledek
            assert TB_OUTPUT = CF
                report "CHYBA"
                severity error;
        end loop;
        report "KONEC SIMULACE" severity note;
        wait;
     end process INPUT_GEN;
end TB_AES_BLOCK_BODY;
