library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity SUB is
port (
    INPUT  : in  STD_LOGIC_VECTOR (127 downto 0);
    OUTPUT : out STD_LOGIC_VECTOR (127 downto 0)
);
end SUB;

architecture SUB_BODY of SUB is
    component SBOX is
        port ( 
            INPUT  : in  STD_LOGIC_VECTOR (7 downto 0);
            OUTPUT : out STD_LOGIC_VECTOR (7 downto 0)
        );
    end component SBOX;
begin
    SUB_BYTES : for I in 1 to 16 generate
        SUB_BYTE: SBOX
            port map (
                INPUT  => INPUT (I*8 - 1 downto (I - 1)*8),
                OUTPUT => OUTPUT(I*8 - 1 downto (I - 1)*8)
            );
    end generate SUB_BYTES;
end SUB_BODY;
