library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity REG is
port (
    INPUT  : in  STD_LOGIC_VECTOR (127 downto 0);
    CLK    : in  STD_LOGIC;
    RST    : in  STD_LOGIC;
    LOAD   : in  STD_LOGIC;
    OUTPUT : out STD_LOGIC_VECTOR (127 downto 0)
);
end REG;

architecture REG_BODY of REG is
    signal CURR_STATE : STD_LOGIC_VECTOR(127 downto 0);
begin
    
    REG_LOAD: process (CLK)
    begin
        if CLK = '1' and CLK'event then
            if RST = '1' then
                CURR_STATE <= (others => '0');
            elsif LOAD = '1' then
                CURR_STATE <= INPUT;
            end if;
        end if;
    end process REG_LOAD;
    OUTPUT <= CURR_STATE;
end REG_BODY;
