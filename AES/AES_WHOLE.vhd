library IEEE;
use IEEE.STD_LOGIC_1164.ALL;


entity AES_WHOLE is
port (
    CLK      : in  STD_LOGIC; 
    RESET    : in  STD_LOGIC;
    BTN0     : in  STD_LOGIC;
    BTN1     : in  STD_LOGIC;
    INPUT    : in  STD_LOGIC_VECTOR (  7 downto 0);
    SWITCHES : in  STD_LOGIC_VECTOR (  2 downto 0);
    LEDS     : out STD_LOGIC_VECTOR ( 15 downto 0);
    SEGMENT  : out STD_LOGIC_VECTOR (6 downto 0);    -- 7 segmentu displeje
    DP       : out STD_LOGIC;                        -- desetinna tecka
    DIGIT    : out STD_LOGIC_VECTOR (3 downto 0)     -- 4 cifry displeje
);
end AES_WHOLE;

architecture AES_WHOLE_BODY of AES_WHOLE is

    component FLIP_FLOP is
    port (
        CLK : in  STD_LOGIC;
        D   : in  STD_LOGIC;
        Q   : out STD_LOGIC
    );
    end component FLIP_FLOP;
    
    component AES is
    port (
        CLK          : in  STD_LOGIC;                        
        RESET        : in  STD_LOGIC;                        
        BTN0         : in  STD_LOGIC;                        
        BTN1         : in  STD_LOGIC;                        
        INPUT        : in  STD_LOGIC_VECTOR (  7 downto 0);  
        INPUT_LAST16 : out STD_LOGIC_VECTOR ( 15 downto 0);  
        OUTPUT       : out STD_LOGIC_VECTOR (127 downto 0)   
     );
    end component AES;
    
    component HEX2SEG is
       port (
          DATA     : in  STD_LOGIC_VECTOR (15 downto 0);   -- vstupni data k zobrazeni (4 sestnactkove cislice)
          CLK      : in  STD_LOGIC;
          SEGMENT  : out STD_LOGIC_VECTOR (6 downto 0);    -- 7 segmentu displeje
          DP       : out STD_LOGIC;                        -- desetinna tecka
          DIGIT    : out STD_LOGIC_VECTOR (3 downto 0)     -- 4 cifry displeje
       );
    end component HEX2SEG;
    
    signal CIPHER_TEXT                  : STD_LOGIC_VECTOR (127 downto 0);
    signal OUTPUT_PART                  : STD_LOGIC_VECTOR ( 15 downto 0);  
    signal DEBOUNCE_0, DEBOUNCE_1, DEBOUNCE_2, DEBOUNCE_3 : STD_LOGIC;
    
begin

    D1 : FLIP_FLOP
        port map (
            CLK => CLK,
            D   => BTN0,
            Q   => DEBOUNCE_1
        );
        
    D2 : FLIP_FLOP
        port map (
            CLK => CLK,
            D   => DEBOUNCE_1,
            Q   => DEBOUNCE_2
        );     
        
    D3 : FLIP_FLOP
        port map (
            CLK => CLK,
            D   => BTN1,
            Q   => DEBOUNCE_0
        );
        
    D4 : FLIP_FLOP
        port map (
            CLK => CLK,
            D   => DEBOUNCE_0,
            Q   => DEBOUNCE_3
        );     
           
    AES_BLOCK : AES
        port map (
            CLK          => CLK,
            RESET        => RESET,
            BTN0         => DEBOUNCE_2,
            BTN1         => DEBOUNCE_3,
            INPUT        => INPUT,
            INPUT_LAST16 => LEDS,
            OUTPUT       => CIPHER_TEXT
        );
    
    HEX2SEG_BLOCK : HEX2SEG
        port map (
            DATA    => OUTPUT_PART,
            CLK     => CLK,
            SEGMENT => SEGMENT,
            DP      => DP,
            DIGIT   => DIGIT
        );
        
    MUX : process (SWITCHES, CIPHER_TEXT)
    begin
        case SWITCHES is
            when "000"  => OUTPUT_PART <= CIPHER_TEXT(127 downto 112);
            when "001"  => OUTPUT_PART <= CIPHER_TEXT(111 downto  96);
            when "010"  => OUTPUT_PART <= CIPHER_TEXT( 95 downto  80); 
            when "011"  => OUTPUT_PART <= CIPHER_TEXT( 79 downto  64); 
            when "100"  => OUTPUT_PART <= CIPHER_TEXT( 63 downto  48); 
            when "101"  => OUTPUT_PART <= CIPHER_TEXT( 47 downto  32); 
            when "110"  => OUTPUT_PART <= CIPHER_TEXT( 31 downto  16); 
            when others => OUTPUT_PART <= CIPHER_TEXT( 15 downto   0); 
        end case;
    end process MUX;

end AES_WHOLE_BODY;
