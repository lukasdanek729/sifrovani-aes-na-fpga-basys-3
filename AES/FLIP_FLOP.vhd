--DFFPR process borrowed from PNO0203vhdl.pdf
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity FLIP_FLOP is
port (
    CLK : in  STD_LOGIC;
    D   : in  STD_LOGIC;
    Q   : out STD_LOGIC
);
end FLIP_FLOP;

architecture FLIP_FLOP_BODY of FLIP_FLOP is  
begin

    DFFPR : process (CLK)
    begin
        if CLK='1' then
            Q <= D;
        end if;
    end process DFFPR;

end FLIP_FLOP_BODY;
