library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity CONTROLLER is
port (
    CLK      : in  STD_LOGIC;
    RESET    : in  STD_LOGIC;
    BTN0     : in  STD_LOGIC;
    BTN1     : in  STD_LOGIC;
    LOAD     : out STD_LOGIC;
    START    : out STD_LOGIC;
    FINAL    : out STD_LOGIC;
    RND      : out STD_LOGIC;
    --FINISHED : out STD_LOGIC;
    RST_DATA : out STD_LOGIC;
    RND_NMBR : out STD_LOGIC_VECTOR (3 downto 0)
);
end CONTROLLER;

architecture CONTROLLER_BODY of CONTROLLER is
    type STATES is (WAITING, LD1, LD2, AES1, AES2);
    signal STATE, NEXT_STATE             : STATES;
    signal CNT_LOAD_UP, CNT_ROUND_UP     : STD_LOGIC; 
    signal CNT_LOAD_INIT, CNT_ROUND_INIT : STD_LOGIC; 
    signal CNT_LOAD_FULL, CNT_ROUND_FULL : STD_LOGIC;
    signal CNT_LOAD                      : STD_LOGIC_VECTOR(4 downto 0);
    signal CNT_ROUND                     : STD_LOGIC_VECTOR(3 downto 0);    
begin
--------------------------------------------------------------------------
-- counters for loading and rounds                                      --
--------------------------------------------------------------------------
    LOAD_COUNTER : process (CLK)
    begin
        if CLK = '1' and CLK'event then
            if CNT_LOAD_INIT = '1'then
                CNT_LOAD <= (others => '0');
            elsif CNT_LOAD_UP = '1' then
                --increase counter
                CNT_LOAD <= STD_LOGIC_VECTOR(UNSIGNED(CNT_LOAD)+ 1);
            end if;
        end if;
    end process LOAD_COUNTER;
    
    CHECK_LOADING : process (CNT_LOAD)
    begin
        if CNT_LOAD = "10000" then
            CNT_LOAD_FULL <= '1';
        else
            CNT_LOAD_FULL <= '0';
        end if;
    end process CHECK_LOADING;
    
    
    ROUND_COUNTER : process (CLK)
    begin
        if CLK = '1' and CLK'event then
            if CNT_ROUND_INIT = '1'then
                CNT_ROUND <= (others => '0');
            elsif CNT_ROUND_UP = '1' then
                --increase counter
                CNT_ROUND <= STD_LOGIC_VECTOR(UNSIGNED(CNT_ROUND)+ 1);
            end if;
        end if;
    end process ROUND_COUNTER;
    
    CHECK_LAST_ROUND : process (CNT_ROUND)
    begin
        if CNT_ROUND = "1010" then -- mozna potom jen 8, od nuly do osmi
            CNT_ROUND_FULL <= '1';
        else
            CNT_ROUND_FULL <= '0';
        end if;
    end process CHECK_LAST_ROUND;
--------------------------------------------------------------------------
-- automata processes                                                   --
--------------------------------------------------------------------------
    TRANSITIONS : process (STATE, BTN0, BTN1, CNT_LOAD_FULL, CNT_ROUND_FULL, RESET)
    begin
        NEXT_STATE <= STATE;
        
        case STATE is
            when WAITING =>
                if BTN0 = '1' then
                    NEXT_STATE <= LD1;
                else
                    NEXT_STATE <= WAITING;
                end if;      
                          
            when LD1 =>
                if BTN1 = '1' then
                    NEXT_STATE <= LD2;
                else
                    NEXT_STATE <= LD1;
                end if;         
                       
            when LD2 =>
                if BTN0 = '1' and CNT_LOAD_FULL = '0' then
                    NEXT_STATE <= LD1;
                elsif CNT_LOAD_FULL = '1' then
                    NEXT_STATE <= AES1;
                else
                    NEXT_STATE <= LD2;
                end if;               
                
            when AES1    =>
                if CNT_ROUND_FULL = '1' then
                    NEXT_STATE <= AES2;
                else
                    NEXT_STATE <= AES1;
                end if;   
                                                           
            when AES2  => 
                NEXT_STATE <= AES2;      
        end case;
    end process TRANSITIONS;
    
    OUTPUTS : process (STATE, BTN0, BTN1, CNT_LOAD_FULL, CNT_ROUND_FULL, RESET)
    begin
        CNT_LOAD_INIT  <= '0';
        CNT_ROUND_INIT <= '0';
        CNT_LOAD_UP    <= '0';
        CNT_ROUND_UP   <= '0';
        RST_DATA       <= '0';
        LOAD           <= '0';
        RND            <= '0';
        START          <= '0';
        FINAL          <= '0';
        --FINISHED       <= '0';
        case STATE is
            when WAITING =>
                if BTN0 = '1' then
                    LOAD           <= '1';
                    CNT_LOAD_UP    <= '1';
                else
                    CNT_LOAD_INIT  <= '1';
                    CNT_ROUND_INIT <= '1';
                    --START          <= '1';
                    RST_DATA       <= '1';
                end if;
                            
            when LD1 =>
                    --START          <= '1';
                if BTN1 = '1' then
                    LOAD           <= '1';
                    CNT_LOAD_UP    <= '1';
                end if;            
                            
            when LD2 =>
                --START          <= '1';
                if BTN0 = '1' and CNT_LOAD_FULL = '0' then
                    LOAD           <= '1';
                    CNT_LOAD_UP    <= '1';
                elsif CNT_LOAD_FULL = '1' then
                    RND            <= '1';
                    CNT_ROUND_UP   <= '1';
                    START          <= '1';
                end if;            
            
            when AES1     =>
                if CNT_ROUND_FULL = '1' then
                    RND            <= '1';
                    FINAL          <= '1';
                    --CNT_ROUND_UP   <= '1';
                elsif CNT_ROUND_FULL = '0' then
                    RND            <= '1';
                    CNT_ROUND_UP   <= '1';
                end if;       
            
            when others  => null;
        end case;
    end process OUTPUTS;
    
    REG_STATE : process (CLK)
    begin
        if CLK = '1' and CLK'event then
            if RESET = '1' then
                STATE <= WAITING;
            else
                STATE <= NEXT_STATE;
            end if;
        end if;
     end process REG_STATE;
     
     ASSIGN_RND : RND_NMBR <= CNT_ROUND;
     
end CONTROLLER_BODY;
