library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use WORK.KEY.ALL;

entity DATAPATH is
port (
    CLK          : in  STD_LOGIC;
    RESET        : in  STD_LOGIC;
    LOAD         : in  STD_LOGIC;
    START        : in  STD_LOGIC;
    FINAL        : in  STD_LOGIC;
    RND          : in  STD_LOGIC;
    RND_NMBR     : in  STD_LOGIC_VECTOR (  3 downto 0);
    INPUT        : in  STD_LOGIC_VECTOR (  7 downto 0);
    INPUT_LAST16 : out STD_LOGIC_VECTOR ( 15 downto 0);
    OUTPUT       : out STD_LOGIC_VECTOR (127 downto 0)
);
end DATAPATH;

architecture DATAPATH_BODY of DATAPATH is
    component MUX is
    port (
        INPUT_0 : in  STD_LOGIC_VECTOR(127 downto 0);
        INPUT_1 : in  STD_LOGIC_VECTOR(127 downto 0);
        SEL     : in  STD_LOGIC;
        OUTPUT  : out STD_LOGIC_VECTOR(127 downto 0)
    );
    end component MUX;
    
    component SHIFT_8_REG is
    port (
        INPUT        : in  STD_LOGIC_VECTOR (7 downto 0);
        LOAD         : in  STD_LOGIC;
        RESET        : in  STD_LOGIC;
        CLK          : in  STD_LOGIC;
        INPUT_LAST16 : out STD_LOGIC_VECTOR ( 15 downto 0);
        OUTPUT       : out STD_LOGIC_VECTOR (127 downto 0)
    );
    end component SHIFT_8_REG;
    
    component REG is
    port (
        INPUT  : in  STD_LOGIC_VECTOR (127 downto 0);
        CLK    : in  STD_LOGIC;
        RST    : in  STD_LOGIC;
        LOAD   : in  STD_LOGIC;
        OUTPUT : out STD_LOGIC_VECTOR (127 downto 0)
    );
    end component REG;
    
    component SUB is
    port (
        INPUT  : in  STD_LOGIC_VECTOR (127 downto 0);
        OUTPUT : out STD_LOGIC_VECTOR (127 downto 0)
    );
    end component SUB;
    
    component SHIFT_RWS is
    port (
        INPUT  : in  STD_LOGIC_VECTOR (127 downto 0);
        OUTPUT : out STD_LOGIC_VECTOR (127 downto 0)
    );
    end component SHIFT_RWS;
    
    component MIX_COLS is
    port (
        INPUT  : in  STD_LOGIC_VECTOR (127 downto 0);
        OUTPUT : out STD_LOGIC_VECTOR (127 downto 0)
    );
    end component MIX_COLS;
    
    component KEY_EXPAND is
    port ( 
        INPUT  : in  STD_LOGIC_VECTOR (127 downto 0);
        ROUND  : in  STD_LOGIC_VECTOR (  3 downto 0);
        OUTPUT : out STD_LOGIC_VECTOR (127 downto 0)
    );
    end component KEY_EXPAND;
    
    signal PL_TXT, MUX_TXT, XOR_TXT, RND_TXT, SUB_TXT, SH_TXT, MIX_TXT, NEXT_TXT : STD_LOGIC_VECTOR (127 downto 0);
    signal RND_KEY, PREV_KEY, NEXT_KEY                                           : STD_LOGIC_VECTOR (127 downto 0);
begin
    
    PLT_REG : SHIFT_8_REG
        port map (
            INPUT        => INPUT,
            LOAD         => LOAD,
            RESET        => RESET,
            CLK          => CLK,
            INPUT_LAST16 => INPUT_LAST16,
            OUTPUT       => PL_TXT
        );
    
    MX_TXT : MUX
        port map (
            INPUT_0 => NEXT_TXT,
            INPUT_1 => PL_TXT,
            SEL     => START,
            OUTPUT  => MUX_TXT    
        );
    
    TXT_XOR_KEY : XOR_TXT <= MUX_TXT xor RND_KEY;
    
    CFR_REG : REG
        port map (
            INPUT  => XOR_TXT,
            CLK    => CLK,
            RST    => RESET,
            LOAD   => RND,
            OUTPUT => RND_TXT      
        );
    
    SUBSTITUTE : SUB
        port map (
            INPUT  => RND_TXT,
            OUTPUT => SUB_TXT
        );
        
    SHIFT_ROWS : SHIFT_RWS
        port map (
            INPUT  => SUB_TXT,
            OUTPUT =>  SH_TXT
        );
        
    MIX_COLUMNS : MIX_COLS
        port map (
            INPUT  => SH_TXT,
            OUTPUT => MIX_TXT
        );
        
    MX_MIX : MUX
        port map (
            INPUT_0 => MIX_TXT,
            INPUT_1 => SH_TXT,
            SEL     => FINAL,
            OUTPUT  => NEXT_TXT           
        );
        
    MX_KEY : MUX
        port map (
            INPUT_0 => NEXT_KEY,
            INPUT_1 => FIRST_KEY,
            SEL     => START,
            OUTPUT  => RND_KEY             
        );
        
    KEY_REG : REG
        port map (
            INPUT  => RND_KEY,
            CLK    => CLK,
            RST    => RESET,
            LOAD   => RND,
            OUTPUT => PREV_KEY
        );
        
    KEY_EXP : KEY_EXPAND
        port map (
        INPUT  => PREV_KEY,
        ROUND  => RND_NMBR,
        OUTPUT => NEXT_KEY
        );   

    GET_OUTPUT : OUTPUT <= RND_TXT;
        
end DATAPATH_BODY;
