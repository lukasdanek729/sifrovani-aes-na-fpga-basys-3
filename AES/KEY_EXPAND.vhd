library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity KEY_EXPAND is
port ( 
    INPUT  : in  STD_LOGIC_VECTOR (127 downto 0);
    ROUND  : in  STD_LOGIC_VECTOR (  3 downto 0);
    OUTPUT : out STD_LOGIC_VECTOR (127 downto 0)
);
end KEY_EXPAND;

architecture KEY_EXPAND_BODY of KEY_EXPAND is
    signal W0, W1, W2, W3, T : STD_LOGIC_VECTOR (31 downto 0);
    
    component MAKE_T is
        port (
            INPUT  : in  STD_LOGIC_VECTOR (31 downto 0);
            ROUND  : in  STD_LOGIC_VECTOR ( 3 downto 0);
            OUTPUT : out STD_LOGIC_VECTOR (31 downto 0)
        );
    end component MAKE_T;

begin

    T_GEN : MAKE_T
        port map(
        INPUT  => INPUT (31 downto 0),
        ROUND  => ROUND,
        OUTPUT => T
        );
    --mozna kazdy slovo zvlast    
    --EXPAND : process (INPUT, T, W0, W1, W2, W3)
    --begin
    MAKE_W0  : W0     <= T  xor INPUT (127 downto 96);
    MAKE_W1  : W1     <= W0 xor INPUT ( 95 downto 64);
    MAKE_W2  : W2     <= W1 xor INPUT ( 63 downto 32);
    MAKE_W3  : W3     <= W2 xor INPUT ( 31 downto  0);
    MAKE_KEY : OUTPUT <= W0 & W1 & W2 & W3;
    --end process EXPAND;

end KEY_EXPAND_BODY;