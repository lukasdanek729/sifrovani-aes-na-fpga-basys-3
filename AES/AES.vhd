library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity AES is
port (
    CLK          : in  STD_LOGIC; 
    RESET        : in  STD_LOGIC;
    BTN0         : in  STD_LOGIC;
    BTN1         : in  STD_LOGIC;
    INPUT        : in  STD_LOGIC_VECTOR (  7 downto 0);
    INPUT_LAST16 : out STD_LOGIC_VECTOR ( 15 downto 0);
    OUTPUT       : out STD_LOGIC_VECTOR (127 downto 0)
);
end AES;

architecture AES_BODY of AES is
    component CONTROLLER is
    port ( 
        CLK      : in  STD_LOGIC;
        RESET    : in  STD_LOGIC;
        BTN0     : in  STD_LOGIC;
        BTN1     : in  STD_LOGIC;
        LOAD     : out STD_LOGIC;
        START    : out STD_LOGIC;
        FINAL    : out STD_LOGIC;
        RND      : out STD_LOGIC;
        RST_DATA : out STD_LOGIC;
        RND_NMBR : out STD_LOGIC_VECTOR (3 downto 0)
    );
    end component CONTROLLER;
    
    component DATAPATH is
    port (
        CLK          : in  STD_LOGIC;
        RESET        : in  STD_LOGIC;
        LOAD         : in  STD_LOGIC;
        START        : in  STD_LOGIC;
        FINAL        : in  STD_LOGIC;
        RND          : in  STD_LOGIC;
        RND_NMBR     : in  STD_LOGIC_VECTOR (  3 downto 0);
        INPUT        : in  STD_LOGIC_VECTOR (  7 downto 0);
        INPUT_LAST16 : out STD_LOGIC_VECTOR ( 15 downto 0);
        OUTPUT       : out STD_LOGIC_VECTOR (127 downto 0)
    );
    end component DATAPATH;
    
    signal W_LOAD, W_START, W_FINAL, W_RND, W_RST_DATA : STD_LOGIC;
    signal W_RND_NMBR                                              : STD_LOGIC_VECTOR(3 downto 0);
begin

    CONT : CONTROLLER
        port map (
            CLK      => CLK,
            RESET    => RESET,
            BTN0     => BTN0,
            BTN1     => BTN1,
            LOAD     => W_LOAD,
            START    => W_START,
            FINAL    => W_FINAL,
            RND      => W_RND,
            RST_DATA => W_RST_DATA,
            RND_NMBR => W_RND_NMBR
        ); 
        
    DATA : DATAPATH
        port map (
            CLK          => CLK,
            RESET        => W_RST_DATA,
            LOAD         => W_LOAD,
            START        => W_START,
            FINAL        => W_FINAL,
            RND          => W_RND,
            RND_NMBR     => W_RND_NMBR,
            INPUT        => INPUT,
            INPUT_LAST16 => INPUT_LAST16,
            OUTPUT       => OUTPUT
        );

end AES_BODY;
