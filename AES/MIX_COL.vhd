library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity MIX_COL is
port (
    INPUT  : in  STD_LOGIC_VECTOR (31 downto 0);
    OUTPUT : out STD_LOGIC_VECTOR (31 downto 0)
);
end MIX_COL;

architecture MIX_COL_BODY of MIX_COL is
    signal B0xorB1, B1xorB2, B2xorB3, B3xorB0                 : STD_LOGIC_VECTOR (7 downto 0);
    signal MUL_B0xorB1, MUL_B1xorB2, MUL_B2xorB3, MUL_B3xorB0 : STD_LOGIC_VECTOR (7 downto 0); 
    
    component GF_MULT is
    port (
        INPUT  : in  STD_LOGIC_VECTOR (7 downto 0);
        OUTPUT : out STD_LOGIC_VECTOR (7 downto 0)    
    );
    end component GF_MULT;
begin
    BXxorBY : process (INPUT)
    begin
        B0xorB1 <= INPUT(31 downto 24) xor INPUT(23 downto 16);
        B1xorB2 <= INPUT(23 downto 16) xor INPUT(15 downto  8);
        B2xorB3 <= INPUT(15 downto  8) xor INPUT( 7 downto  0);
        B3xorB0 <= INPUT( 7 downto  0) xor INPUT(31 downto 24);
    end process BXxorBY; 
    
    MUL1 : GF_MULT
        port map (
            INPUT  =>     B0xorB1,
            OUTPUT => MUL_B0xorB1
        );
    MUL2 : GF_MULT
        port map (
            INPUT  =>     B1xorB2,
            OUTPUT => MUL_B1xorB2
        ); 
    MUL3 : GF_MULT
        port map (
            INPUT  =>     B2xorB3,
            OUTPUT => MUL_B2xorB3
        );
    MUL4 : GF_MULT
        port map (
            INPUT  =>     B3xorB0,
            OUTPUT => MUL_B3xorB0
        ); 
    MAKE_C0 : OUTPUT(31 downto 24) <=  MUL_B0xorB1 xor B1xorB2 xor INPUT( 7 downto  0); --2(B0 + B1) + B1 + B2 + B3 
    MAKE_C1 : OUTPUT(23 downto 16) <=  MUL_B1xorB2 xor B2xorB3 xor INPUT(31 downto 24); --2(B1 + B2) + B2 + B3 + B0 
    MAKE_C2 : OUTPUT(15 downto  8) <=  MUL_B2xorB3 xor B3xorB0 xor INPUT(23 downto 16); --2(B2 + B3) + B3 + B0 + B1 
    MAKE_C3 : OUTPUT( 7 downto  0) <=  MUL_B3xorB0 xor B0xorB1 xor INPUT(15 downto  8); --2(B3 + B0) + B0 + B1 + B2 
end MIX_COL_BODY;
