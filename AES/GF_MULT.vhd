library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity GF_MULT is
port (
    INPUT  : in  STD_LOGIC_VECTOR (7 downto 0);
    OUTPUT : out STD_LOGIC_VECTOR (7 downto 0) 
);
end GF_MULT;

architecture GF_MULT_BODY of GF_MULT is
    signal LEFT_SHIFT : STD_LOGIC_VECTOR (7 downto 0);
begin
    LEFT_SHIFT <= INPUT (6 downto 0) & '0';
    
    MULTIPLY : process (INPUT, LEFT_SHIFT)
    begin
        if INPUT(7) = '1' then
            OUTPUT <= LEFT_SHIFT xor "00011011";
        else
            OUTPUT <= LEFT_SHIFT;
        end if;
    end process MULTIPLY;
end GF_MULT_BODY;
